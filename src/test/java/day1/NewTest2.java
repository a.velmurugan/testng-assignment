package day1;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NewTest2 {
  @Test
  public class NewTest1 {
	  @Test	(dependsOnMethods = "newcustomer")
	  public void modifycustomer() {
		  System.out.println("The customer will get modified");
	  }
	  @Test(dependsOnMethods ="modifycustomer" )
	  public void createcustomer() {
		  System.out.println("The customer will get created");
	  }
	  @Test
	  public void newcustomer() {
		  System.out.println("The customer will get created");
	  }
	  @BeforeMethod
	  public void beforecustomer() {
		  System.out.println("Verifying customer");
	  }
	  @AfterMethod
	  public void aftercustomer() {
		  System.out.println("All the trancastions are done");
	  }
	  @BeforeClass
	  public void beforeclassmethod() {
		  System.out.println("start Database connection,Lunch Browser");
	  }
	  @AfterClass
	  public void afterclassmethod() {
		  System.out.println("close Database connection,close Browser");
	  }
	  
	}

  }

