package Assignment;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class actTime {
	static WebDriver driver;
  @BeforeMethod
  public void launch() {
	 // WebDriver driver = new ChromeDriver();
	  driver.get("https://demo.actitime.com/login.do");
	  driver.manage().window().maximize();
  }
  @Test(priority = 1)
  public void text() {
	  WebElement a= driver.findElement(By.id("headerContainer"));
	  a.click();
	  String text = a.getText();
	  System.out.println(text);
	  String actual = text;
	 String expect = "Please identify yourself";
	 if(actual.equals(expect)) {
		 System.out.println("text is present");
	 }
	 else {
		 System.out.println("text is not present");
	 }
  }
  @Test(priority = 2)
  public void logo() {
	WebElement logo = driver.findElement(By.xpath("//div[@class='atLogoImg']"));
	if(logo.isDisplayed()) {
		System.out.println("logo Displayed");
		
	}
	else {
		System.out.println("Not Displayed");
	}
  }

@AfterMethod
public void close() {
	driver.quit();
}

}